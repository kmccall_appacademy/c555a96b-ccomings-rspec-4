class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(new_entries)
    if new_entries.is_a?(String)
      @entries[new_entries] = nil
    elsif new_entries.is_a?(Hash)
      @entries.merge!(new_entries)
    end
  end

  def keywords
    @entries.keys.sort {|x,y| x <=> y}
  end

  def include?(x)
    @entries.keys.include?(x)
  end

  def find(word_part)
    @entries.select do |word|
      word.match(word_part)
    end
  end

  def printable
    entries = keywords.map do |keyword|
      %Q([#{keyword}] "#{@entries[keyword]}")
    end
    entries.join("\n")
  end

end
