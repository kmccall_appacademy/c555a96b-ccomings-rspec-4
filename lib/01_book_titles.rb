class Book
  BAD_WORDS = %w[and in the of a to an]

  attr_reader :title

  def title=(title)
    title_words = title.split
    title_words.each do |word|
      word.capitalize! unless BAD_WORDS.include?(word)
    end
    title_words[0].capitalize!
    @title = title_words.join(' ')
  end
end
