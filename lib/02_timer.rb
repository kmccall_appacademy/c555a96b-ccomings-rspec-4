class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def double_digits(num)
    if num > 9
      "#{num}"
    else
      "0#{num}"
    end
  end

  def hours
    Integer(seconds / 3600)
  end

  def mins
    Integer((seconds % 3600) / 60)
  end

  def remainder_sec
    Integer(seconds % 60)
  end

  def time_string
    "#{double_digits(hours)}:#{double_digits(mins)}:#{double_digits(remainder_sec)}"
  end
end
